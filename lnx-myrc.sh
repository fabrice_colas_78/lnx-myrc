#!/bin/bash

GIT_REPO=https://gitlab.com/fabrice_colas_78/lnx-myrc.git

COMMON_PACKAGES="git"

which git >/dev/null
has_git=$?

if [ $has_git -ne 0 ]; then
  echo "Error: git tool is not installed in this box, exiting..."
  exit 1
fi

mkdir -p ~/.local && cd ~/.local
if [ ! -d lnx-myrc ]; then
  git clone $GIT_REPO
  if [ $? -ne 0 ]; then
    echo "Error: Unable to clone ${GIT_REPO}"
    exit 1
  fi
  cd lnx-myrc
else
  cd lnx-myrc
  git pull
fi

echo ""
if [[ $SHELL =~ "bash" ]]; then echo -e "Don't forget to reload your 'bash' configuration with:\n\e[35msource ~/.bashrc\e[0m"; fi
if [[ $SHELL =~ "zsh" ]]; then echo -e "Don't forget to reload your 'zsh' configuration with:\n\e[35msource ~/.zshrc\e[0m"; fi
echo ""
