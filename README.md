## Linux `myrc`

```bash
# Basic functions without sudo
curl -s https://gitlab.com/api/v4/projects/15486379/repository/files/lnx-tools.sh/raw?ref=master | NO_INSTALL_COMMON_PACKAGES=1 WITH_VIM_AIRLINE=1 bash
curl -s -L https://gitlab.com/fabrice_colas_78/lnx-myrc/-/raw/master/lnx-myrc.sh | bash

```

```bash
# Basic functions with sudo
curl -s https://gitlab.com/api/v4/projects/15486379/repository/files/lnx-tools.sh/raw?ref=master | bash
curl -s -L https://gitlab.com/fabrice_colas_78/lnx-myrc/-/raw/master/lnx-myrc.sh | bash

```

```bash
# Basic functions + vim_airline
curl -s https://gitlab.com/api/v4/projects/15486379/repository/files/lnx-tools.sh/raw?ref=master | WITH_VIM_AIRLINE=1 bash
curl -s -L https://gitlab.com/fabrice_colas_78/lnx-myrc/-/raw/master/lnx-myrc.sh | bash

```
